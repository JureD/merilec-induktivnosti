double kapacitivnost = 2 * pow(10, -6);

double cas1;
double cas2;
double cas3;
double cas;


double induktivnost;
int prikazi = 0;

int inputPin = 10;
int outputPin = 9;
int buttonPin = 3;

void setup() {
  Serial.begin(9600);
  
  pinMode(inputPin, INPUT);
  pinMode(outputPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(buttonPin), meritev, RISING);

  Serial.println("Induktometer:");
  Serial.println("Pritisni tipko za merjenje!");
  delay(1000);
}

void loop() {
  if (prikazi == 1) {
    cas = (cas1 + cas2 + cas3) / 3;
    induktivnost = 1000 / (4.0 * pow(3.14159265359, 2) * pow((pow(10, 6) / (2 * cas)), 2) * kapacitivnost);
    prikazi = 0;
    Serial.print("\033[2J");
    Serial.print("\033[H");
    Serial.print("Induktivnost tuljave je: ");
    Serial.print(induktivnost);
    Serial.println(" mH");
    delay(1000);
  }
   

}

void meritev() {
  prikazi = 1;
  digitalWrite(outputPin, HIGH);
  delay(5);
  digitalWrite(outputPin, LOW);
  delayMicroseconds(100);
  cas1 = pulseIn(inputPin, HIGH, 5000000);
  delay(250);
  digitalWrite(outputPin, HIGH);
  delay(5);
  digitalWrite(outputPin, LOW);
  delayMicroseconds(100);
  cas2 = pulseIn(inputPin, HIGH, 5000000);
  delay(250);
  digitalWrite(outputPin, HIGH);
  delay(5);
  digitalWrite(outputPin, LOW);
  delayMicroseconds(100);
  cas3 = pulseIn(inputPin, HIGH, 5000000);
}
