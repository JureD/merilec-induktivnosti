EESchema Schematic File Version 4
LIBS:induktometer-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328-PU U1
U 1 1 5BD9A6F1
P 2100 4300
F 0 "U1" H 1459 4346 50  0000 R CNN
F 1 "ATmega328-PU" H 1459 4255 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 2100 4300 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 2100 4300 50  0001 C CNN
	1    2100 4300
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4001 D1
U 1 1 5BD9A7D8
P 4800 3200
F 0 "D1" H 4800 3416 50  0000 C CNN
F 1 "1N4001" H 4800 3325 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4800 3025 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 4800 3200 50  0001 C CNN
	1    4800 3200
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5BD9A8BF
P 4700 1700
F 0 "#PWR01" H 4700 1550 50  0001 C CNN
F 1 "+5V" H 4715 1873 50  0000 C CNN
F 2 "" H 4700 1700 50  0001 C CNN
F 3 "" H 4700 1700 50  0001 C CNN
	1    4700 1700
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR02
U 1 1 5BD9A905
P 5300 6300
F 0 "#PWR02" H 5300 6050 50  0001 C CNN
F 1 "Earth" H 5300 6150 50  0001 C CNN
F 2 "" H 5300 6300 50  0001 C CNN
F 3 "~" H 5300 6300 50  0001 C CNN
	1    5300 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BD9A9E0
P 3650 5100
F 0 "R2" V 3443 5100 50  0000 C CNN
F 1 "10k" V 3534 5100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3580 5100 50  0001 C CNN
F 3 "~" H 3650 5100 50  0001 C CNN
	1    3650 5100
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5BD9AA70
P 2850 2450
F 0 "R1" H 2920 2496 50  0000 L CNN
F 1 "10k" H 2920 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2780 2450 50  0001 C CNN
F 3 "~" H 2850 2450 50  0001 C CNN
	1    2850 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5BD9AB2B
P 6950 3800
F 0 "R4" H 7020 3846 50  0000 L CNN
F 1 "390" H 7020 3755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6880 3800 50  0001 C CNN
F 3 "~" H 6950 3800 50  0001 C CNN
	1    6950 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5BD9ABA3
P 4250 3200
F 0 "R3" V 4043 3200 50  0000 C CNN
F 1 "180" V 4134 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4180 3200 50  0001 C CNN
F 3 "~" H 4250 3200 50  0001 C CNN
	1    4250 3200
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5BD9AC70
P 3300 3750
F 0 "Y1" V 3254 3881 50  0000 L CNN
F 1 "16MHz" V 3345 3881 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-U_Vertical" H 3300 3750 50  0001 C CNN
F 3 "~" H 3300 3750 50  0001 C CNN
	1    3300 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5BD9AD20
P 3900 3600
F 0 "C2" V 3648 3600 50  0000 C CNN
F 1 "22p" V 3739 3600 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 3938 3450 50  0001 C CNN
F 3 "~" H 3900 3600 50  0001 C CNN
	1    3900 3600
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5BD9ADAE
P 3900 3900
F 0 "C3" V 4152 3900 50  0000 C CNN
F 1 "22p" V 4061 3900 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D6.0mm_W2.5mm_P5.00mm" H 3938 3750 50  0001 C CNN
F 3 "~" H 3900 3900 50  0001 C CNN
	1    3900 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 5BD9AE22
P 5500 5750
F 0 "C4" H 5615 5796 50  0000 L CNN
F 1 "1uF" H 5615 5705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L31.5mm_W11.0mm_P27.50mm_MKS4" H 5538 5600 50  0001 C CNN
F 3 "~" H 5500 5750 50  0001 C CNN
	1    5500 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5BD9AE9A
P 5800 5750
F 0 "C5" H 5915 5796 50  0000 L CNN
F 1 "1uF" H 5915 5705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L31.5mm_W11.0mm_P27.50mm_MKS4" H 5838 5600 50  0001 C CNN
F 3 "~" H 5800 5750 50  0001 C CNN
	1    5800 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5BD9AF1A
P 3150 4500
F 0 "C1" V 2898 4500 50  0000 C CNN
F 1 "100pF" V 2989 4500 50  0000 C CNN
F 2 "" H 3188 4350 50  0001 C CNN
F 3 "~" H 3150 4500 50  0001 C CNN
	1    3150 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 3700 3000 3700
Wire Wire Line
	3000 3700 3000 3600
Wire Wire Line
	3000 3600 3300 3600
Wire Wire Line
	2700 3800 3000 3800
Wire Wire Line
	3000 3800 3000 3900
Wire Wire Line
	3000 3900 3300 3900
Wire Wire Line
	3750 3600 3300 3600
Connection ~ 3300 3600
Wire Wire Line
	3750 3900 3300 3900
Connection ~ 3300 3900
Wire Wire Line
	2100 5800 2100 6100
Wire Wire Line
	4700 2050 4700 1700
Wire Wire Line
	2100 2800 2100 2050
Wire Wire Line
	5300 6300 5300 6100
Wire Wire Line
	2200 2800 2200 2050
Wire Wire Line
	2850 2050 2850 2300
Wire Wire Line
	2850 4600 2700 4600
Wire Wire Line
	3800 5100 4050 5100
Wire Wire Line
	3500 5100 2700 5100
Wire Wire Line
	4400 3200 4650 3200
$Comp
L Comparator:LM2903 U2
U 1 1 5BDA1498
P 6650 4550
F 0 "U2" H 6650 4917 50  0000 C CNN
F 1 "LM2903" H 6650 4826 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 6650 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 6650 4550 50  0001 C CNN
	1    6650 4550
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U2
U 3 1 5BDA1551
P 6700 4550
F 0 "U2" H 6658 4596 50  0000 L CNN
F 1 "LM2903" H 6658 4505 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 6700 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393-n.pdf" H 6700 4550 50  0001 C CNN
	3    6700 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 5900 5500 6100
Wire Wire Line
	5800 5900 5800 6100
Wire Wire Line
	5500 5600 5800 5600
Wire Wire Line
	2700 3200 4100 3200
Wire Wire Line
	4950 3200 4950 5600
Wire Wire Line
	4950 5600 5500 5600
Connection ~ 5500 5600
Wire Wire Line
	5500 4450 5500 5600
Wire Wire Line
	5800 5600 8200 5600
Connection ~ 5800 5600
Wire Wire Line
	5500 4450 6350 4450
Wire Wire Line
	6350 4650 6350 6100
Wire Wire Line
	2100 6100 4050 6100
Connection ~ 5300 6100
Wire Wire Line
	5300 6100 5500 6100
Connection ~ 5500 6100
Wire Wire Line
	5500 6100 5800 6100
Connection ~ 5800 6100
Wire Wire Line
	5800 6100 6350 6100
Wire Wire Line
	2100 2050 2200 2050
Connection ~ 2200 2050
Wire Wire Line
	2200 2050 2850 2050
Connection ~ 2850 2050
Wire Wire Line
	2850 2050 4700 2050
Connection ~ 4700 2050
Wire Wire Line
	4700 2050 6600 2050
Wire Wire Line
	6600 4850 6600 6100
Wire Wire Line
	6600 6100 6350 6100
Connection ~ 6350 6100
Wire Wire Line
	6600 4250 6600 3450
Wire Wire Line
	6950 4550 6950 3950
Wire Wire Line
	6950 3650 6950 3450
Wire Wire Line
	6950 3450 6600 3450
Connection ~ 6600 3450
Wire Wire Line
	6600 3450 6600 2050
Connection ~ 6600 6100
Wire Wire Line
	2700 3300 7400 3300
Wire Wire Line
	7400 3300 7400 4550
Wire Wire Line
	7400 4550 6950 4550
Connection ~ 6950 4550
Wire Wire Line
	4050 5100 4050 6100
Connection ~ 4050 6100
Wire Wire Line
	4050 6100 5300 6100
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5BDAF5A3
P 3850 4400
F 0 "J1" H 3823 4330 50  0000 R CNN
F 1 "Conn_01x03_Male" H 3823 4421 50  0000 R CNN
F 2 "" H 3850 4400 50  0001 C CNN
F 3 "~" H 3850 4400 50  0001 C CNN
	1    3850 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4400 3500 4400
Wire Wire Line
	3500 4400 3500 4900
Wire Wire Line
	3500 4900 2700 4900
Wire Wire Line
	3650 4300 3400 4300
Wire Wire Line
	3400 4300 3400 4800
Wire Wire Line
	3400 4800 2700 4800
Wire Wire Line
	3000 4500 2850 4500
Connection ~ 2850 4500
Wire Wire Line
	2850 4500 2850 4600
Wire Wire Line
	2850 2600 2850 4500
Wire Wire Line
	3300 4500 3650 4500
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5BDBE2DF
P 8600 6100
F 0 "J2" H 8573 5980 50  0000 R CNN
F 1 "Conn_01x02_Male" H 8573 6071 50  0000 R CNN
F 2 "" H 8600 6100 50  0001 C CNN
F 3 "~" H 8600 6100 50  0001 C CNN
	1    8600 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 5600 8200 6000
Wire Wire Line
	8200 6000 8400 6000
Wire Wire Line
	6600 6100 8400 6100
$EndSCHEMATC
